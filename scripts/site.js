var hamburgerBtn = document.querySelector('.hamburger.menu')
var siteMenu = document.querySelector('#site-navigation')
hamburgerBtn.addEventListener('click', function (argument) {
    siteMenu.classList.add('open')
}, true)
var hamburgerBtnClose = document.querySelector('.hamburger.menu-close')
hamburgerBtnClose.addEventListener('click', function (argument) {
    siteMenu.classList.remove('open')
}, true)