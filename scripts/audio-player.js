var audio = document.getElementById("player");
var artist = document.querySelector('.playback-track .artist');
var track = document.querySelector('.playback-track .track');

var MEDIALIST = document.querySelectorAll('.media-list li');
var CURRENTTRACKINDEX = 0;

track.innerText = MEDIALIST[CURRENTTRACKINDEX].getAttribute('data-title');
artist.innerText = MEDIALIST[CURRENTTRACKINDEX].getAttribute('data-artist-name');
audio.src = MEDIALIST[CURRENTTRACKINDEX].getAttribute('data-src');


function getNextTrack () {
    return {
        'title': MEDIALIST[CURRENTTRACKINDEX+1].getAttribute('data-title'),
        'artist-name': MEDIALIST[CURRENTTRACKINDEX+1].getAttribute('data-artist-name'),
        'album-title': MEDIALIST[CURRENTTRACKINDEX+1].getAttribute('data-album-title'),
        'src': MEDIALIST[CURRENTTRACKINDEX+1].getAttribute('data-src')
    }
}
function getPrevTrack () {
    return {
        'title': MEDIALIST[CURRENTTRACKINDEX-1].getAttribute('data-title'),
        'artist-name': MEDIALIST[CURRENTTRACKINDEX-1].getAttribute('data-artist-name'),
        'album-title': MEDIALIST[CURRENTTRACKINDEX-1].getAttribute('data-album-title'),
        'src': MEDIALIST[CURRENTTRACKINDEX-1].getAttribute('data-src')
    }
}

function playPrevTrack () {
    try {
        var prevTrack = getPrevTrack();
        artist.innerText = prevTrack['artist-name'];
        track.innerText = prevTrack['title'];
        audio.src = prevTrack['src'];
        audio.play();
        CURRENTTRACKINDEX -= 1;
    } catch(e) {
        // statements
        console.log('no next/prev track to play', e);
    }
}
function playNextTrack () {
    try {
        var nextTrack = getNextTrack();
        artist.innerText = nextTrack['artist-name'];
        track.innerText = nextTrack['title'];
        audio.src = nextTrack['src'];
        audio.play();
        CURRENTTRACKINDEX += 1;
    } catch(e) {
        // statements
        console.log('no next/prev track to play', e);
    }
}


// Countdown
audio.addEventListener("timeupdate", function() {
    var timeleft = document.querySelector('.playback-time-remaining'),
        duration = parseInt( audio.duration ),
        currentTime = parseInt( audio.currentTime ),
        timeLeft = duration - currentTime,
        s, m;
    
    
    s = timeLeft % 60;
    m = Math.floor( timeLeft / 60 ) % 60;
    
    s = s < 10 ? "0"+s : s;
    m = m < 10 ? "0"+m : m;
    
    timeleft.innerHTML = (m||"00")+":"+(s||"00");
    
}, false);

// Countup
audio.addEventListener("timeupdate", function() {
    var timeline = document.querySelector('.playback-time');
    var s = parseInt(audio.currentTime % 60);
    var m = parseInt((audio.currentTime / 60) % 60);
    if (s < 10) {
        timeline.innerHTML = m + ':0' + s;
    }
    else {
        timeline.innerHTML = m + ':' + s;
    }
}, false);

// Play
audio.addEventListener("play", function() {
    playBtn.innerText = 'PAUSE';
    console.log('song played');
}, false);

// End
audio.addEventListener("ended", function() {
    playBtn.innerText = 'PLAY';
    playNextTrack();
    console.log('song ended');
}, false);



// Media Info
var mediaInfoWrapper = document.querySelector('.media-info');
var artistName = mediaInfoWrapper.querySelector('.artist');
var trackName = mediaInfoWrapper.querySelector('.track');


// Playback
var playBtn = document.querySelector('.playback-controls .play');
playBtn.addEventListener('click', function (e) {
    if (audio.paused) {
        audio.play();
        playBtn.innerText = 'PAUSE';
        return;
    }
    audio.pause();
    playBtn.innerText = 'PLAY';
}, true);
var prevBtn = document.querySelector('.playback-controls .prev');
prevBtn.addEventListener('click', function (e) {
    playPrevTrack();
}, true);
var nextBtn = document.querySelector('.playback-controls .next');
nextBtn.addEventListener('click', function (e) {
    playNextTrack();
}, true);
