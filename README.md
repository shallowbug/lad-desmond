# README #

DESMOND FOR LADDESIGN

### OZZY AND ALZY ###

LET'S GET STARTED

FIRST put the project folder in another folder:

`lad-desmond/lad-desmond/..`


### INSTALL ###

Install Docker for Windows
https://docs.docker.com/docker-for-windows/install/

Install Node and NPM
https://nodejs.org/en/download/

Install Node dependencies
In project root run `npm install`


### SETUP ###

Run this in a cmd window (cd into the project directory first)

`docker-compose up`

This will run the docker image, install wordpress and mount the `wp-content` folder in the parent folder: `lad-desmond/docker-volume/wp-content`

Go to `localhost:8000` in your browser and finish the wordpress install

Now run `link-theme.bat` to create a symlink of the current directory to the wordpress themes directory. The theme should now show up in the Wordpress admin panel. Activate it.


### LESS DO DIS ###


Run this in another cmd window

`gulp`

SICK ASS FOO! Now editting and saving your files refreshes the browser automatically.
NOTE: If a browser window doesn't automatically open when you run gulp, manually load the page by going to localhost:3000 in your browser

Remember to activate the theme in the wordpress admin panel first.


### NOTE ###

We're using a Wordpress plugin(?) called typerocket. Read about it here
https://typerocket.com/docs/v4/

It's essentially an extension that converts the stock Wordpress API into something more closer to Laravel. It's pretty dope. If you look at the `functions.php` file you'll see that `music.functions.php` and `gallery.functions.php` are imported. I brought them over from a previous file from reference. They create custom menus in the backend (see the left sidebar in the backend, there is `Music` and `Galleries` options.)