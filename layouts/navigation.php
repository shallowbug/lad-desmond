<div class="page-container" data-is-homepage="<?php echo (is_front_page() ? 'true' : 'false') ?>">
    <!-- header -->
    <header>
        <a href="/" class="title-link">
            <h1 class="title">DESMOND CHILD & ROUGE</h1>
        </a>
        <button class="hamburger menu">
            <img src="<?php echo get_template_directory_uri(); ?>/images/hamburger.png">
        </button>
    </header>
</div>
<div id="site-navigation">
    <nav>
        <h1 class="title">DESMOND CHILD & ROUGE</h1>
        <button class="hamburger menu-close">
            <img src="<?php echo get_template_directory_uri(); ?>/images/hamburger.png">
        </button>
        <ul>
            <li>
                <a href="/">HOME</a>
            </li>
            <li>
                <a href="/about">ABOUT</a>
            </li>
            <li>
                <a href="/music">MUSIC</a>
            </li>
            <li>
                <a href="/gallery">GALLERY</a>
            </li>
        </ul>
    </nav>
</div>
