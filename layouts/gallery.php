<div class="gallery-page">

    <div class="header">
        <p>
            Tower dead smart-towards carbon girl range-rover warehouse semiotics. Apophenia realism bomb motion warehouse stimulate jeans camera computer.
        </p>
    </div>

    <div class="hero-wrapper row">
        <div class="hero-side"></div>

        <div class="hero row">
            <img src="<?php echo get_template_directory_uri(); ?>/images/dev/gallery/gallery-header.jpg">

            <div class="gallery-text column">
                <p>
                    Chrome Legba long-chain hydrocarbons digital dolphin range-rover sentient tanto paranoid wonton soup math.
                </p>
                <p>
                    Apophenia realism bomb
                    <br>1978
                </p>
            </div>
        </div>

        <div class="hero-side hero-right"></div>
    </div>

    <div class="image-grid">
        <?php while (have_posts()) : the_post() ?>
            <div class="image-wrapper">
                <?php echo tr_posts_field(':img:small:hero_image'); ?>

                <div class="hero-content">
                    <?php echo tr_posts_field(':img:full:detail_image'); ?>
                    <div class="gallery-text">                    
                        <?php
                            echo tr_posts_field('detail_text');
                        ?>
                    </div>
                </div>
            </div>
        <?php endwhile ?>
    </div>
</div>

<script type="text/javascript">
var heroFloater = document.querySelector('.gallery-page-hero-wrapper');
var heroFloaterContent = heroFloater.querySelector('.gallery-page-hero');

var images = document.querySelectorAll('.image-wrapper');
for (var i = 0; i < images.length; i++){
    var image = images[i];
    var imageHeroContent = image.querySelector('.hero-content');
    image.onclick = function () {
        heroFloaterContent.innerHTML = imageHeroContent.innerHTML;
        heroFloater.classList.add('open');
    }
}

heroFloater.onclick = function () {
    heroFloater.classList.remove('open');
}
</script>