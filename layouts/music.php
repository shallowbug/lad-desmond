<div class="music-page">
<?php while (have_posts()) : the_post() ?>
<div class="row music-section">
    <div class="column bio">
        <?php echo tr_posts_field('bottom_text'); ?>
    </div>
    <div class="column listen">
        <h2>LISTEN</h2>
        <div id="player">
            <div class="row track-details">
                <div class="cover column"></div>
                <div class="title column">
                    <span>DESMOND CHILD & ROUGE</span>
                </div>
            </div>
            <div class="tracks">
                <ol class="media-list">
                    <?php
                        $links = get_post_meta( get_the_ID(), 'music_details', true );
                        if ($links):
                        foreach ($links as $link):
                            $song_url = $link['song_file'];
                            $song_url = wp_get_attachment_url($song_url);
                            $album_image = $link['album_image'];
                            $album_image = wp_get_attachment_image_src($album_image, 'small')[0];
                    ?>
                        <li
                        data-album-title="<?php echo $link['album_title'] ?>"
                        data-album-image="<?php echo $album_image ?>"
                        data-title="<?php echo $link['song_title'] ?>"
                        data-src="<?php echo $song_url ?>"
                        >
                            <?php echo $link['song_title'] ?>
                        </li>
                    <?php endforeach; endif; ?>
                </ol>
            </div>
        </div>
    </div>
    <div class="column album-covers">
    <?php
        $images = get_post_meta( get_the_ID(), 'side_images', true );
        if ($images):
        foreach ($images as $image):
            $imagesSrcs = wp_get_attachment_image_src($image, 'large')[0];
    ?>
        <img src="<?php echo $imagesSrcs ?>">
    <?php endforeach; endif; ?>
    </div>
</div>
<div class="row video-section">
    <?php
        $videos = get_post_meta( get_the_ID(), 'videos', true );
        if ($videos):
        foreach ($videos as $video):
            $videosSrc = wp_get_attachment_url($video['video']);
    ?>
        <div class="video">
            <video controls>
                <source src="<?php echo $videosSrc ?>" type="video/mp4">
                Your browser does not support the video tag.
            </video>
        </div>
    <?php endforeach; endif; ?>
</div>
<?php break; endwhile; ?>
</div>