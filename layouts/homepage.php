<div class="home-links">
    <div class="row hero">
        <div class="column">
            <?php
                $image = get_post_meta( get_the_ID(), 'hero_image', true );
                if ($image):
                    $image = wp_get_attachment_image_src($image, 'large')[0]
            ?>
                <img src="<?php echo $image; ?>" class="framed medium homepage-hero-img">
            <?php else: ?>
                <img src="<?php echo get_template_directory_uri(); ?>/images/dev/hero.png" class="framed medium homepage-hero-img">
            <?php endif; ?>

        </div>    
        <div class="column hero-text">    
            <?php
            the_post();
            the_content();
            ?>
        </div>
    </div>

    <div class="row second-line">
        <a href="/about">
            <div class="column">
                <h3 class="frame-title">ABOUT</h3>
                <img src="<?php echo get_template_directory_uri(); ?>/images/dev/about.jpg"
                class="no-top-padding">
            </div>
        </a>
        <a href="/music">
            <div class="column">
                <h3 class="frame-title">MUSIC</h3>
                <img src="<?php echo get_template_directory_uri(); ?>/images/dev/music.jpg"
                class="no-top-padding">
            </div>
        </a>
    </div>

    <div class="row">
        <a href="/music">
            <div class="column video-frame">
                <h3 class="frame-title">VIDEO</h3>
                <div class="play-button">
                    <div class="play-triangle"></div>
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/images/dev/video.jpg"
                class="no-top-padding">
            </div>
        </a>
        <a href="/gallery">
            <div class="column">
                <h3 class="frame-title">GALLERY</h3>
                <img src="<?php echo get_template_directory_uri(); ?>/images/dev/gallery.jpg"
                class="no-top-padding">
            </div>
        </a>
    </div>
</div>