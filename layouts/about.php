<div class="about-page">

<?php if ( have_posts() ) : while ( have_posts() ) : ?>
    <div class="row">
        <?php
            echo tr_posts_field(':img:full:hero_image');
        ?>
    </div>

    <div class="page-container">
        <?php
            the_post();
            the_content();
        ?>
    </div>
<?php endwhile; else: ?>
    <div class="page-container">
        <p>This url has no content</p>
    </div>
<?php endif; ?>
</div>