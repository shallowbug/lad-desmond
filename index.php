<!-- IMPORT HEADER -->
<?php get_header(); ?>



<!-- TEMPLATE CONTENT -->
<div class="page-container" data-is-homepage="<?php echo (is_front_page() ? 'true' : 'false') ?>">
    <?php 
    if (is_front_page()) {
        @include('layouts/homepage.php');
    } else {
        @include('layouts/about.php');
    }
    ?>
</div>



<!--  -->
<?php get_footer(); ?>