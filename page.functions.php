<?php 

$boxPages = tr_meta_box('Page Details');
$boxPages->addScreen('page');
$boxPages->setCallback(function(){
    $form = tr_form();

    $hero_image = $form->image('hero_image');  
    echo $hero_image->setLabel('Header Image');

    $bg_image = $form->image('bg_image');
    echo $bg_image->setLabel('Background Image');
});
$boxPages->setPriority('high');
?>