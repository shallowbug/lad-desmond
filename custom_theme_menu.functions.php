<?php

function ellistheme_customize_register( $wp_customize ) {

    $wp_customize->add_section( 'ellistheme_about_section' , array(
        'title'      => __( 'About Section Information', 'ellistheme' ),
        'priority'   => 30,
    ));

    $wp_customize->add_setting( 'ellistheme_about_text', array());
    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'ellistheme_about_text_control',
            array(
                'label'      => __( 'About Text', 'ellistheme' ),
                'type'       => 'textarea',
                'section'    => 'ellistheme_about_section',
                'settings'   => 'ellistheme_about_text',
                'priority'   => 1
            )
        )
    );

    $wp_customize->add_setting( 'ellistheme_about_text_extended', array());
    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'ellistheme_about_text_extended_control',
            array(
                'label'      => __( 'About Text Extended', 'ellistheme' ),
                'type'       => 'textarea',
                'section'    => 'ellistheme_about_section',
                'settings'   => 'ellistheme_about_text_extended',
                'priority'   => 1
            )
        )
    );



    $wp_customize->add_setting( 'ellistheme_about_gallery_description', array());
    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'ellistheme_about_gallery_description_control',
            array(
                'label'      => __( 'About Images', 'ellistheme' ),
                'description'=> 'To select images for the about section go to the galleries section and select them there.',
                'type'       => 'hidden',
                'section'    => 'ellistheme_about_section',
                'settings'   => 'ellistheme_about_gallery_description',
                'priority'   => 1
            )
        )
    );


    // ABOUT CREDITS
    $wp_customize->add_setting( 'ellistheme_about_credit_1', array());
    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'ellistheme_about_credit_1_control',
            array(
                'label'      => __( 'About Credit 1', 'ellistheme' ),
                'type'       => 'text',
                'section'    => 'ellistheme_about_section',
                'settings'   => 'ellistheme_about_credit_1',
                'priority'   => 1
            )
        )
    );

    $wp_customize->add_setting( 'ellistheme_about_credit_2', array());
    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'ellistheme_about_credit_2_control',
            array(
                'label'      => __( 'About Credit 2', 'ellistheme' ),
                'type'       => 'text',
                'section'    => 'ellistheme_about_section',
                'settings'   => 'ellistheme_about_credit_2',
                'priority'   => 1
            )
        )
    );

    $wp_customize->add_setting( 'ellistheme_about_credit_3', array());
    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'ellistheme_about_credit_3_control',
            array(
                'label'      => __( 'About Credit 3', 'ellistheme' ),
                'type'       => 'text',
                'section'    => 'ellistheme_about_section',
                'settings'   => 'ellistheme_about_credit_3',
                'priority'   => 1
            )
        )
    );

    $wp_customize->add_setting( 'ellistheme_about_credit_4', array());
    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'ellistheme_about_credit_4_control',
            array(
                'label'      => __( 'About Credit 4', 'ellistheme' ),
                'type'       => 'text',
                'section'    => 'ellistheme_about_section',
                'settings'   => 'ellistheme_about_credit_4',
                'priority'   => 1
            )
        )
    );

    $wp_customize->add_setting( 'ellistheme_about_credit_5', array());
    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'ellistheme_about_credit_5_control',
            array(
                'label'      => __( 'About Credit 5', 'ellistheme' ),
                'type'       => 'text',
                'section'    => 'ellistheme_about_section',
                'settings'   => 'ellistheme_about_credit_5',
                'priority'   => 1
            )
        )
    );

    // ..repeat ->add_setting() and ->add_control() for ellistheme_about-division
}
add_action( 'customize_register', 'ellistheme_customize_register' );

?>