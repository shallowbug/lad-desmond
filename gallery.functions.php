<?php

$gallery = tr_post_type('Gallery', 'Gallery');
$gallery->setId('tr_gallery');
$gallery->setIcon('eye');
$gallery->setArgument('supports', ['title'] );

$gallery->setEditorForm( function () {
    $form = tr_form();

    $heroImage = $form->image('hero_image');
    echo $heroImage->setLabel('Hero Image');

    $detailImage = $form->image('detail_image');
    echo $detailImage->setLabel('Detail Image');

    $detailText = $form->editor('detail_text');
    echo $detailText->setLabel('Detail Text');

});


// Admin Panel Columns
$gallery->addColumn('hero_image', false, 'Image', function($value) {
    echo wp_get_attachment_image($value, 'thumbnail');
}, 'number');

// $gallery->removeColumn('title');
$gallery->removeColumn('date');

// $gallery->setPrimaryColumn('hero_image');
?>
