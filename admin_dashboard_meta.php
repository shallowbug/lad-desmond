<?php

$dashboard_meta = tr_meta_box('Site Info');
$dashboard_meta->addScreen('dashboard');
$dashboard_meta->setCallback(function(){
    echo '
    <div style="padding: 15px;">
        <h2>The <b>Home</b> and <b>About</b> pages can be editted in the Pages section to the left.</h2>
        <h2>The <b>Music</b> and <b>Gallery</b> pages have their own menus in the sidebar</h2>
    </div>
    ';
});
$dashboard_meta->setPriority('high');





function remove_dashboard_widgets() {
    global $wp_meta_boxes;
 
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_activity']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
 
}
 
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );
?>