<?php


// IMPORT TYPEROCKET
require ('typerocket/init.php');

include 'page.functions.php';

include 'music.functions.php';
include 'gallery.functions.php';

include 'admin_dashboard_meta.php';
// include 'custom_theme_menu.functions.php';
include 'remove_default_wp_functions.php';



add_action('after_setup_theme', 'my_setup_theme');
function my_setup_theme() {
    add_theme_support('editor-styles');
    add_editor_style('css/styles-editor.css');
}



function set_post_order_in_admin( $wp_query ) {
    global $pagenow;

    if ( is_admin() && 'edit.php' == $pagenow && !isset($_GET['orderby'])) {

        $wp_query->set( 'orderby', 'menu_order' );
        $wp_query->set( 'order', 'ASC' );       
    }
}
add_filter('pre_get_posts', 'set_post_order_in_admin', 5 );



// ENQUE STYLES/SCRIPTS
if (!is_admin()) {
    $theme = wp_get_theme(); // Used for cache busting
    wp_enqueue_style(
        'Style',
        get_template_directory_uri() . '/css/styles.css',
        array(),
        $theme->get( 'Version' ),
        'all'
    );
}


?>