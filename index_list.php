<ul id="index-list" class="navigation hidden">
<?php 
    $args = array(
    'post_type'=> 'post',
    'orderby'    => 'ID',
    'post_status' => 'publish',
    'order'    => 'DESC',
    'posts_per_page' => -1 // this will retrive all the post that is published 
    );
    $result = new WP_Query( $args );
    if ( $result-> have_posts() ) :
?>
<?php while ( $result->have_posts() ) : $result->the_post(); ?>
    <?php
        $images = get_post_meta( get_the_ID(), 'gallery_pictures', true );
        if (!$images) {$images = [];} // account for no images.
        $imagesSrcs = '';
        foreach ($images as $image) {
            if (!$imagesSrcs == ''){
                $imagesSrcs = $imagesSrcs . ', ';
            }
            $imagesSrcs = $imagesSrcs . wp_get_attachment_image_src($image, 'large')[0];
        }
    ?>
    
    <li data-img-src="<?php echo $imagesSrcs ?>" data-page-uri="<?php echo get_page_uri(get_the_id()); ?>" data-hover="<?php echo esc_attr( get_post_meta( get_the_ID(), 'index_hover_animation', true ) ); ?>">
        <div class="summary">
            <h2><?php echo the_title(); ?></h2>
            <h3><?php echo the_date(); ?></h3>
            <h4><?php echo esc_attr( get_post_meta( get_the_ID(), 'short_desc', true ) ); ?></h4>
        </div>
        <div class="page" data-content='<?php the_content(); ?>'>
        </div>
        <div class="bottom-left-headers">
            <ul class="credits-detail-list">
                <?php
                    $credits = get_post_meta( get_the_ID(), 'credits', true );
                    if ($credits):
                    foreach ($credits as $credit):
                ?>
                    <li>
                        <?php print_r($credit['credits']) ?>
                    </li>
                <?php endforeach; endif; ?>
            </ul>
        </div>
        <div class="floating-left-vertical-text">
            <p>Some Text On Top</p>
            <p>Some Text On Bottom</p>
        </div>
        <div class="links-wrapper hidden">
            <ul class="links">
            <?php
                $links = get_post_meta( get_the_ID(), 'links', true );
                if ($links):
                foreach ($links as $link):
            ?>
                <li><a href="<?php echo $link['link_url'] ?>" target="_blank">
                    <?php echo $link['link_name'] ?>
                </a></li>
            <?php endforeach; endif; ?>
            </ul>
        </div>
    </li>

<?php endwhile; ?>
<?php endif; ?>
</ul>