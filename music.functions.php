<?php

$music = tr_post_type('Music', 'Music');
$music->setId('tr_music');
$music->setIcon('headphones');
$music->setArgument('supports', ['title'] );
add_post_type_support( 'tr_music', 'page-attributes' );
$music_meta = tr_meta_box('Music Page Content')->apply($music);


$music_meta->setCallback(function() {
    $form = tr_form();

    $music_repeater = $form->repeater('music_details')->setFields([
        $form->text('Album Title'),
        $form->file('Album Image'),
        $form->text('Song Title'),
        $form->file('Song File')
    ]);
    $music_repeater->setLabel('Music');
    echo $music_repeater;



    echo '<h3>Page Content</h3>';
    $bottom_text = $form->editor('bottom_text');
    echo $bottom_text->setLabel('Bottom Text');

    $side_images = $form->gallery('side_images');
    echo $side_images->setLabel('Side Images');

    $video_repeater = $form->repeater('Videos')->setFields([
        $form->file('Video')
    ]);
    echo $video_repeater;

});

?>